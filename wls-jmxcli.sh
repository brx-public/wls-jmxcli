#!/usr/bin/env bash
USAGE="$(basename $0) HOSTNAME PORT COMMAND [PARAMETERS]"
HOSTNAME="$1"
shift

PORT="$1"
shift

export CLASSPATH=$CLASSPATH

if [[ -z "$PORT" ]]; then
  echo "$USAGE"
  exit 1;
fi
JAR="jmxcli-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
JAVA_OPTIONS="$JAVA_OPTIONS -Djava.util.logging.config.file=logging.properties"
$JAVA_HOME/bin/java $JAVA_OPTIONS \
    -DHOST="$HOSTNAME" -DPORT="$PORT" \
    -DSECURITY_PRINCIPAL="weblogic" \
    -DSECURITY_CREDENTIALS="welcome1" \
    -jar target/$JAR $@
