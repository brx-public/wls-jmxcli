# WebLogic JMXCli

A very simple command line tool to interact with MBeans via JMX.
It has been intended to be used with WebLogic Sever, but it could work with any MBeanServer.

### Running 

Run the tool using:

	java -DHOST=${SERVER_HOST}-DPORT=${SERVER_PORT} -DSECURITY_PRINCIPAL=${USERNAME} -DSECURITY_CREDENTIALS=${PASSWORD} -jar target/jmxcli-0.0.1-SNAPSHOT-jar-with-dependencies.jar ${COMMAND} ${PARAMETERS}
	
This will run the `Main` class.

Note that there are other `main()` methods in other classes for specific reports: 

 - JMSUtils listConsumers: prints JMS consumers
 - Utils printTransactions: prints running transactions 

### Usage

The `COMMAND`(s) available mimic the methods of the `MBeanServerConnection ` object:

 - `queryNames PATTERN`
 - `getMBeanInfo MBEANNAME`
 - `getAttribute MBEANNAME|PATTERN ATTRIBUTENAME`
 - `getAttributes MBEANNAME|PATTERN`


### Building

First of all you have to build `wlfullclient.jar` from you `WL_HOME/server/lib` directory by running

    java -jar wljarbuilder.jar

Then import to your local maven repository:

    mvn install:install-file -DgroupId=weblogic -DartifactId=wlfullclient -Dversion=12.1.3 -Dfile=wlfullclient.jar -Dpackaging=jar

Then create the runnable jar with:

    mvn package

this will build a self-contained (including `wlfullclient`) runnable jar.

### References

[MBeanServerConnection (Java Platform SE 7 )][MBeanServerConnectionAPI]


[MBeanServerConnectionAPI]: https://docs.oracle.com/javase/7/docs/api/javax/management/MBeanServerConnection.html#getMBeanInfo(javax.management.ObjectName)