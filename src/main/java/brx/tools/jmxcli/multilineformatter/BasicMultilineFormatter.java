package brx.tools.jmxcli.multilineformatter;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.management.ObjectInstance;
import brx.tools.jmxcli.Formatter;

public class BasicMultilineFormatter extends Formatter {
	String outTemplate = "${name} ${attributeName} = ${attributeValue}\n";
	String SEP = " ";
	String INDENT = "\t";


	private Map<String, Object> attrMap = new TreeMap<String, Object>();
	private String header = null;
	private long timeStamp;

	public BasicMultilineFormatter(){
//		LOGGER = Logger.getLogger(this.getClass().getName());
		LOGGER.finer("<CONSTRUCTOR> ENTER");
	}

	public String getTemplate(){
		return outTemplate;
	}

	protected void addLine(String name, String value){
		this.attrMap.put(name, value);
	}

	protected void addHeader(String value){
		this.header = value;
	}

	protected void addHeader(ObjectName value){
		this.header = value.getCanonicalName();
	}

	protected void addTimestamp(long value){
		this.timeStamp = value;
	}

	protected String getHeader(){
		return this.header;
	}

	public String format(ObjectName objectName, String attributeName, String attributeValue, long timeStampMillis){
		this.addHeader(objectName);
		this.addLine(attributeName, attributeValue);
		this.addTimestamp(timeStampMillis);
		return this.format();
	}

	public String format(ObjectName objectName, Map<String, Object> map, long timeStampMillis){
		return this.format(objectName.getCanonicalName(), map, timeStampMillis);
	}

	public String format(String objectName, Map<String, Object> map, long timeStampMillis){
		LOGGER.finer("(String, Map, long) ENTER");
		StringBuffer out = new StringBuffer();
		LOGGER.finer("  objectName=" + objectName);
		LOGGER.finer("  map=" + map);
		LOGGER.finer("  map.entrySet()=" + map.entrySet());
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			Object value = entry.getValue();
			String svalue = "null";
			if (value != null) svalue = value.toString();
			out.append(this.formatDateStamp(timeStampMillis) + objectName + SEP + entry.getKey() + "=" + svalue + "\n");
		}
		LOGGER.finer("EXIT");
		return out.toString();
	}
	
	public String format(){
		if ((this.header == null) || (this.attrMap.isEmpty())){
			throw new java.lang.RuntimeException("Missing values for header or attrMap.");
		}
		return this.format(this.header, this.attrMap, this.timeStamp);
	}

	public String format(ObjectInstance object, long timeStampNS){
		LOGGER.warning("* not implemented yet *");
		return "";
	}

}
