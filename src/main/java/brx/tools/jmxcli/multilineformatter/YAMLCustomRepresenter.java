package brx.tools.jmxcli.multilineformatter;

import java.util.Set;
import java.util.TreeMap;

import javax.management.ObjectName;
import javax.management.openmbean.CompositeDataSupport;

import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Represent;
import org.yaml.snakeyaml.representer.Representer;
import java.util.logging.Logger;


public class YAMLCustomRepresenter extends Representer {

	private static Logger LOGGER = Logger.getLogger(YAMLCustomRepresenter.class.getName());
    public YAMLCustomRepresenter() {
            this.representers.put(javax.management.ObjectName.class, new RepresentObjectName());
            this.representers.put(javax.management.openmbean.CompositeDataSupport.class, new RepresentCompositeDataSupport());
        }

        private class RepresentObjectName implements Represent {
            public Node representData(Object data) {
                
                String value = ((ObjectName) data).getCanonicalName();
                LOGGER.fine("RepresentObjectName - '" + value + "'.");
                return representScalar(new Tag("!ObjectName"), value);
            }
        }
        
        private class RepresentCompositeDataSupport implements Represent{
            public Node representData(Object data) {
                TreeMap<String, Object> map = new TreeMap<String, Object>();
                Set<String> keys = ((CompositeDataSupport) data).getCompositeType().keySet();
                for ( String key : keys) {
                    Object value = ((CompositeDataSupport) data).get(key);
                    map.put(key, value);
                }
                return representMapping(new Tag("!CompositeData"), map, defaultFlowStyle);
            }
        }
}
