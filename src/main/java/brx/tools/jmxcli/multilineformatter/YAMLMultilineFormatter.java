package brx.tools.jmxcli.multilineformatter;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.management.ObjectInstance;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import brx.tools.jmxcli.lineformatter.BasicFormatter;


public class YAMLMultilineFormatter extends BasicMultilineFormatter {
	private static Logger LOGGER = Logger.getLogger(YAMLMultilineFormatter.class.getName());
	private String withTimestamp = System.getProperty("Formatter.TimeStamp");
    
    private DumperOptions yamlOpt = new DumperOptions();
    private Yaml yaml;
    
    public YAMLMultilineFormatter(){
        yamlOpt.setDefaultFlowStyle(DumperOptions.FlowStyle.AUTO);
        yamlOpt.setPrettyFlow(true);
        this.yaml = new Yaml(new YAMLCustomRepresenter(), yamlOpt);
    };

	public String format(ObjectName objectName, Map<String, Object> attrMap, long timeStampNS){
	    LOGGER.finer("ENTER");
	    TreeMap<String, Object> map = new TreeMap<String, Object>();
	    map.put(objectName.getCanonicalName(),attrMap);
	    LOGGER.finer("EXIT");
		return yaml.dump(map);
	}

	public String format(ObjectInstance object, long timeStampNS){
	    LOGGER.finer("ENTER");
	    LOGGER.finer("EXIT");
		return yaml.dump(object);
	}

}
