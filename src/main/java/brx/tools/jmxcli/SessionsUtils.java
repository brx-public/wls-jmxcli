package brx.tools.jmxcli;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.management.ObjectName;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import brx.tools.jmxcli.multilineformatter.YAMLCustomRepresenter;

public class SessionsUtils {
	static RemoteMBeanClient remoteMBS;

	static String USAGE = "Usage: brx.tools.jmxcli.SessionsUtils METHOD.\n  valid methods are: ";

	static String MBEAN_JTARUNTIME = "com.bea:*,Type=JTARuntime";
	
	static String[] COMMANDS = {
		"getRoleExpression"
	};
	
	private static Logger LOGGER = Logger.getLogger(Utils.class.getName());
	
	private static void list(String mBeanFilter) {
		LOGGER.fine("ENTER - list()");
		
		String MBEAN_QUERY = brx.tools.jmxcli.Utils.composeMBeanQueryWithFilter("com.bea:Type=WebAppComponentRuntime", mBeanFilter);
	
		Set<ObjectName> mbeans = remoteMBS.queryNames(MBEAN_QUERY);
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				//System.out.println(remoteMBS.getMBeanInfo(mbeanName));
				// Map<String, Object> attrVals = remoteMBS.getAllAttributesValues(mbeanName);
				// String out = brx.tools.jmxcli.Utils.printMap(attrVals);
				// System.out.println(out);
				String[] monitoringIds = (String[]) remoteMBS.getAttribute(mbeanName, "ServletSessionsMonitoringIds");

				for (String mId : monitoringIds){
					Object[] params = new Object[] { mId };
					String[] signature = new String[] { "java.lang.String" };
					long lastAccessedTime = (long) remoteMBS.invoke(
							mbeanName, 
							"getSessionLastAccessedTime", 
							params, 
							signature);
					long maxInactiveInterval = (long) remoteMBS.invoke(
							mbeanName, 
							"getSessionMaxInactiveInterval", 
							params, 
							signature);
					Calendar cal = Calendar.getInstance();
					long now = cal.getTimeInMillis();
					cal.setTimeInMillis(lastAccessedTime);
					Date tla = cal.getTime();
					long idle = now - cal.getTimeInMillis();
					System.out.println(mId + " lastAccessedTime='" + tla + "' idle=" + (idle / 1000) + " maxInactiveInterval=" + maxInactiveInterval);
				}
				
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		String SECURITY_PRINCIPAL = System.getProperty("SECURITY_PRINCIPAL");
		String SECURITY_CREDENTIALS = System.getProperty("SECURITY_CREDENTIALS");
		String HOST = System.getProperty("HOST");
		String PORT = System.getProperty("PORT");
		String mbeanServer = System.getProperty("MBEANSERVER");

		if (mbeanServer == null) {
			mbeanServer = Constants.MBEANSERVER_DOMAINRUNTIME;
		}
		String serviceURLString = "service:jmx:t3://" + HOST + ":" + PORT + "/jndi/" + mbeanServer;
		String method = "";
		if (args.length > 0) {
			method = args[0];
		}
		try {
			LOGGER.fine("Service URL: " + serviceURLString);
			remoteMBS = new RemoteMBeanClient(serviceURLString, SECURITY_PRINCIPAL, SECURITY_CREDENTIALS);
			LOGGER.info("Connecting to: " + remoteMBS.getServiceUrl());



			switch (method) {
			case "list":
				list(args[1]);
				break;
			default:
				System.err.println("ERROR: method '" + method + "' not implemented yet.");
				System.err.println(USAGE);
				System.err.println("Implemented methods are:");
				for (java.lang.reflect.Method m : JMSUtils.class.getDeclaredMethods()) {
					if (m.getName().startsWith("main") || !(java.lang.reflect.Modifier.isPublic(m.getModifiers()))) {
						continue;
					}
					System.err.print("\t" + m.getName().substring(m.getName().lastIndexOf('.') + 1));
					for (Class p : m.getParameterTypes()) {
						String name = p.getSimpleName();
						// if(p.isNamePresent()){
						// name = p.getName();
						// }
						System.err.print(" " + name);
					}
					System.err.println();
				}
			}
		} catch (Exception e) {
			remoteMBS.close();
			e.printStackTrace();
		}
		remoteMBS.close();
	}
}
