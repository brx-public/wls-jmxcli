package brx.tools.jmxcli;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors

import javax.management.ObjectName;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import brx.tools.jmxcli.multilineformatter.YAMLCustomRepresenter;

public class SecurityInfo {

    static String SECURITY_PRINCIPAL = System.getProperty("SECURITY_PRINCIPAL");
    static String SECURITY_CREDENTIALS = System.getProperty("SECURITY_CREDENTIALS");
    static String HOST = System.getProperty("HOST");
    static String PORT = System.getProperty("PORT");
    static String HOSTSFILE = System.getProperty("HOSTSFILE");
    static String mbeanServer = System.getProperty("MBEANSERVER");

    static RemoteMBeanClient remoteMBS;

    static String USAGE = "Usage: brx.tools.jmxcli.SecurityInfo METHOD.\n ";
    
    static String[] COMMANDS = {
        "getRoleExpression"
    };
    
    private static Logger LOGGER = Logger.getLogger(Utils.class.getName());    
    

    public static String[][] getRoleExpression(String roleType, String roleName, String location){
        String[][] results = new String[128][2];
        LOGGER.fine("ENTER - getRoleExpression()");
        Set<ObjectName> mbeans = remoteMBS.queryNames("Security:Name=myrealmXACMLRoleMapper,Location=" + location);
        if (mbeans != null) {
            int i = 0;
            for (ObjectName mbeanName : mbeans) {
                String name = (String) remoteMBS.getAttribute(mbeanName, "Name");
                Object[] params = new Object[] { roleType, roleName };
                String[] signature = new String[] { "java.lang.String", "java.lang.String" };
                String expression = (String) remoteMBS.invoke(
                            mbeanName, 
                            "getRoleExpression", 
                            params, 
                            signature);
                results[i] = new String[]{mbeanName.toString(), expression};
                i++;
            }
        }
        return results;
    }

    public static void main(String[] args) throws Exception {
        String[] hosts = new String[128];
        String[] ports = new String[128];
        int length = 0;

        if (mbeanServer == null) {
            mbeanServer = Constants.MBEANSERVER_DOMAINRUNTIME;
        }
        if (HOSTSFILE != null) 
        {
            int i = 0;
            try (BufferedReader br = new BufferedReader(new FileReader(HOSTSFILE))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (line != ""){
                        String[] parts = line.split(" ");
                        hosts[i] = parts[0];
                        ports[i] = parts[1];
                        i++;
                        length++;
                    }
                }
            }
        } else {
            hosts[0] = HOST;
            ports[0] = PORT;
            length = 1;
        }
        for (int i=0; i < length; i++){
            String serviceURLString = "service:jmx:t3://" + hosts[i] + ":" + ports[i] + "/jndi/" + mbeanServer;
            String method = "";
            if (args.length > 0){
                method = args[0];
            }
            try {
                LOGGER.fine("Service URL: " + serviceURLString);
                remoteMBS = new RemoteMBeanClient(serviceURLString, SECURITY_PRINCIPAL, SECURITY_CREDENTIALS);
                LOGGER.fine("Connecting to: " + remoteMBS.getServiceUrl());

                switch (method) {
                case "getRoleExpression":
                    String[][] results = getRoleExpression(args[1], args[2], "AdminServer");
                    for (int k = 0; k < results.length; k++)
                    {
                        if ((results[k][0] == null)){
                            break;
                        }
                        System.out.println("\r" + hosts[i] + " " + results[k][0] + " resourceId='" + args[1] + "' roleName='" + args[2] + "' '" + results[k][1] + "'");
                    }
                    break;
                default:
                    System.err.println("ERROR: method '" + method + "' not implemented yet.");
                    System.err.println(USAGE);
                    System.err.println("Implemented methods are:");
                    for (java.lang.reflect.Method m : SecurityInfo.class.getDeclaredMethods()) {
                        if (m.getName().startsWith("main") || !(java.lang.reflect.Modifier.isPublic(m.getModifiers()))) {
                            continue;
                        }
                        System.err.print("\t" + m.getName().substring(m.getName().lastIndexOf('.') + 1));
                        for (Class p : m.getParameterTypes()) {
                            String name = p.getSimpleName();
                            // if(p.isNamePresent()){
                            // name = p.getName();
                            // }
                            System.err.print(" " + name);
                        }
                        System.err.println();
                    }
                }
                remoteMBS.close();
                remoteMBS = null;
            } catch (Exception e) {
                if (remoteMBS != null){
                    remoteMBS.close();
                    remoteMBS = null;
                    e.printStackTrace();
                }
            }
        }
    }
}
