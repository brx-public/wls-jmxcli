package brx.tools.jmxcli;

import java.util.Arrays;
import java.util.Formatter;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.management.ObjectName;
import javax.management.ObjectInstance;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import brx.tools.jmxcli.lineformatter.BasicFormatter;
import brx.tools.jmxcli.multilineformatter.BasicMultilineFormatter;
import brx.tools.jmxcli.multilineformatter.YAMLCustomRepresenter;

// TODO: add a JMXPing feature to just time how long does it take to retrieve an MBean
// TODO: manage java.net.ConnectException: destination unreachable 

public class Main<RemoteMBean> {
	static RemoteMBeanClient remoteMBS;

	static String USAGE = "Usage: brx.tools.jmxcli.Main METHOD.\n  valid methods are: queryNames, getMBeanInfo, getAttribute, invoke";

	static String MBEANSERVER_RUNTIME = "weblogic.management.mbeanservers.runtime";
	static String MBEANSERVER_DOMAINRUNTIME = "weblogic.management.mbeanservers.domainruntime";
	static String MBEANSERVER_EDIT = "weblogic.management.mbeanservers.edit";
	static String DEFAULT_METHOD = "queryNames";
	private static Logger LOGGER = Logger.getLogger(Main.class.getName());
	static private int EXIT_NOT_FOUND = 2;

	static BasicFormatter lineFormatter;
	static BasicMultilineFormatter multilineFormatter;

	public static void queryNames(String[] args) {
		LOGGER.fine("ENTER - queryNames(args)");
		Set<ObjectName> mbeans;
		if (args.length > 1) {
			LOGGER.fine("\targs[1]=" + args[1]);
			mbeans = remoteMBS.queryNames(args[1]);
		} else {
			LOGGER.fine("\targs=NULL");
			mbeans = remoteMBS.queryNames();
		}
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				System.out.println(mbeanName.getCanonicalName());
			}
		}
	}

	public static void getObjectInstance(String[] args) {
		LOGGER.fine("ENTER - getObjectInstance(args)");
		String info;
		if (args.length <= 1) {
			System.out.println("Usage: getObjectInstance ObjectPattern");
			System.exit(1);
		}
		LOGGER.fine("\targs[1]=" + args[1]);
		Set<ObjectName> mbeans = remoteMBS.queryNames(args[1]);
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				LOGGER.fine("\t" + mbeanName);
				long timeStamp = System.currentTimeMillis();
				ObjectInstance bean = remoteMBS.getObjectInstance(mbeanName);
				System.out.println(multilineFormatter.format(bean, timeStamp));
			}
		} else {
			System.exit(EXIT_NOT_FOUND);
		}
		LOGGER.fine("EXIT - getObjectInstance(args)");
	}

	public static void getMBeanInfo(String[] args) {
		LOGGER.fine("ENTER - getMBeanInfo(args)");
		String info;
		if (args.length <= 1) {
			System.out.println("Usage: getMBeanInfo ObjectPattern");
			System.exit(1);
		}
		LOGGER.fine("\targs[1]=" + args[1]);
		Set<ObjectName> mbeans = remoteMBS.queryNames(args[1]);
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				LOGGER.fine("\t" + mbeanName);
				info = remoteMBS.getMBeanInfo(mbeanName);
				System.out.println(info);
			}
		} else {
			System.exit(EXIT_NOT_FOUND);
		}
		LOGGER.fine("EXIT - getMBeanInfo(args)");
	}

	public static void getAttribute(String[] args) {
		Expression exp = null;
		LOGGER.fine("ENTER - getAttribute(args) " + args.length);
		if (args.length <= 2) {
			System.out.println("Usage: getAttribute OBJECTNAME ATTRIBUTE [gt|eq|lt VALUE]");
			System.exit(1);
		}
		LOGGER.fine("\targs[1]=" + args[1]);
		LOGGER.fine("\targs[2]=" + args[2]);
		if (args.length == 5) {
			LOGGER.fine("\targs[3]=" + args[3]);
			LOGGER.fine("\targs[4]=" + args[4]);
			exp = new Expression(args[3], args[4]);
		}
		Set<ObjectName> mbeans = remoteMBS.queryNames(args[1]);
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				long timeStamp = System.currentTimeMillis();
				Object value = remoteMBS.getAttribute(mbeanName, args[2]);
				if ((exp == null) || (exp.eval(value))) {
					System.out.print(lineFormatter.format(mbeanName, args[2], String.valueOf(value), timeStamp));
				}
			}
		}
		LOGGER.fine("EXIT - getAttribute(args)");
	}
	
	public static void ping(String[] args) {
		
		LOGGER.fine("ENTER - ping(args) " + args.length);
		if (args.length <= 1) {
			System.out.println("Usage: ping OBJECTNAME [ATTRIBUTE]");
			System.exit(1);
		}
		String attr = null;
		if (args.length > 2){
			attr = args[2];
		}
		long timeStamp = System.currentTimeMillis();
		Set<ObjectName> mbeans = remoteMBS.queryNames(args[1]);
		long timeQueryName = remoteMBS.timerQueryNames;
		if (mbeans != null) {
			ObjectName mbeanName = mbeans.iterator().next();
			System.out.print(lineFormatter.format(mbeanName, "queryNameTime", String.valueOf(timeQueryName), timeStamp));
			if (attr != null){
				Object value = remoteMBS.getAttribute(mbeanName, attr);
				long timerGetAttribute = remoteMBS.timerGetAttribute;
				System.out.print(lineFormatter.format(mbeanName, "getAttributeTime", String.valueOf(timerGetAttribute), timeStamp));
			}
		}
		LOGGER.fine("EXIT - ping(args)");
	}


	public static void getAttributes(String[] args) {
		LOGGER.fine("ENTER - getAttributeS(args)");
		Set<ObjectName> mbeans;
		if (args.length < 2) {
			System.out.println("Usage: getAttributes ObjectName");
			System.exit(1);
		}
		LOGGER.fine("\targs[1]=" + args[1]);
		String[] args_more = Arrays.copyOfRange(args, 1, args.length);
		LOGGER.fine("\targs[*]=" + args_more);
		mbeans = remoteMBS.queryNames(args[1]);
		DumperOptions yamlOpt = new DumperOptions();
		// yamlOpt.setDefaultFlowStyle(DumperOptions.FlowStyle.AUTO);
		// yamlOpt.setPrettyFlow(true);
		// Yaml yaml = new Yaml(new YAMLCustomRepresenter(), yamlOpt);
		if (mbeans != null) {
			long timeStamp = System.currentTimeMillis();
			for (ObjectName mbeanName : mbeans) {
				LOGGER.fine("\t" + mbeanName);
				Map<String, Object> attrMap;
				if (args_more.length > 1 ) {
					attrMap = new TreeMap<String, Object>(remoteMBS.getAttributesValues(mbeanName, args_more));
				}else {
					attrMap = new TreeMap<String, Object>(remoteMBS.getAllAttributesValues(mbeanName));
				}
				// TODO: Add multiformatter to linearize the output: CSV, influxdb line protocol, etc, etc
				System.out.println(multilineFormatter.format(mbeanName, attrMap, timeStamp));
			}
		}

	}

	public static void main(String[] args) throws Exception {
		String SECURITY_PRINCIPAL = System.getProperty("SECURITY_PRINCIPAL");
		String SECURITY_CREDENTIALS = System.getProperty("SECURITY_CREDENTIALS");
		String HOST = System.getProperty("HOST");
		String PORT = System.getProperty("PORT");
		String mbeanServer = System.getProperty("MBEANSERVER", MBEANSERVER_RUNTIME);
		String serviceURLString = "service:jmx:t3://" + HOST + ":" + PORT + "/jndi/" + mbeanServer;
		String lineFormatterClass = System.getProperty("LineFormatter.Class", "brx.tools.jmxcli.lineformatter.BasicFormatter");
		String multilineFormatterClass = System.getProperty("MultilineFormatter.Class", "brx.tools.jmxcli.multilineformatter.YAMLMultilineFormatter");
		String method = "";
		lineFormatter = (BasicFormatter) Main.class.getClassLoader().loadClass(lineFormatterClass).newInstance();
		multilineFormatter = (BasicMultilineFormatter) Main.class.getClassLoader().loadClass(multilineFormatterClass).newInstance();
		if (args.length > 0) {
			method = args[0];
		}
		try {
			LOGGER.fine("Service URL: " + serviceURLString);
			LOGGER.fine("Line Formatter class: " + lineFormatterClass);
			LOGGER.fine("Multiline Formatter class: " + multilineFormatterClass);
			remoteMBS = new RemoteMBeanClient(serviceURLString, SECURITY_PRINCIPAL, SECURITY_CREDENTIALS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			LOGGER.info("Connecting to: " + remoteMBS.getServiceUrl());
			switch (method) {
			case "queryNames":
				queryNames(args);
				break;
			case "getMBeanInfo":
				getMBeanInfo(args);
				break;
			case "getAttribute":
				getAttribute(args);
				break;
			case "getAttributes":
				getAttributes(args);
				break;
			case "ping":
				ping(args);
				break;
			case "getObjectInstance":
				getObjectInstance(args);
			default:
				System.err.println("ERROR: method '" + method + "' not implemented yet.");
				System.err.println(USAGE);
			}
		} catch (Exception e) {
			remoteMBS.close();
			e.printStackTrace();
		}
		remoteMBS.close();
	}
}
