package brx.tools.jmxcli.lineformatter;

import java.util.HashMap;
import java.util.Map;
import brx.tools.jmxcli.Formatter;
import javax.management.ObjectName;

public class BasicFormatter extends Formatter {

	protected String withTimestamp = System.getProperty("Formatter.TimeStamp");
	protected String withDatestamp = System.getProperty("Formatter.DateStamp");
	protected String namingAttribute = System.getProperty("Formatter.NamingAttribute");

	String outTemplate = "${name} ${attributeName} = ${attributeValue}\n";

	public String format(ObjectName objectName, String attributeName, String attributeValue, long timeStamp){
		return this.format(outTemplate, objectName.getCanonicalName(), attributeName, attributeValue, timeStamp);
	}	
	
	public String format(String name, String attributeName, String attributeValue, long timeStamp){
		return this.format(outTemplate, name, attributeName, attributeValue, timeStamp);
	}
	
	public String format(String template, String name, String attributeName, String attributeValue, long timeStamp){
		Map<String, String> valuesMap = new HashMap<String, String>();
		valuesMap.put("name", name);
		valuesMap.put("attributeName", attributeName);
		valuesMap.put("attributeValue", attributeValue);
		valuesMap.put("timeStamp", String.valueOf(timeStamp));
		return org.apache.commons.lang3.text.StrSubstitutor.replace(template, valuesMap);
	}
	
}
