package brx.tools.jmxcli.lineformatter;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.management.ObjectName;

import brx.tools.jmxcli.Main;

public class InfluxDBLineProtocolFormatter extends BasicFormatter {

	
	private static Logger LOGGER = Logger.getLogger(InfluxDBLineProtocolFormatter.class.getName());
	private String withTimestamp = System.getProperty("Formatter.TimeStamp");
	private String withDatestamp = System.getProperty("Formatter.DateStamp");
	private String namingAttribute = System.getProperty("Formatter.NamingAttribute");

	public String format(ObjectName objectName, String attributeName, String attributeValue, long timeStamp){

		LOGGER.finer(objectName.getCanonicalName());		
		StringBuffer sb = new StringBuffer();
		TreeMap<String, String> m = new TreeMap<String, String>();
		m.putAll(objectName.getKeyPropertyList());;
		String measure = new String();
		String name = "none";
		String attr = "type";
		if (namingAttribute != null && m.containsKey(namingAttribute)){
			attr = namingAttribute;
		} else if (m.containsKey("Type")){
		 	attr = "Type";
		} else if (m.containsKey("type")){
			attr = "type";	
		}
		name = m.get(attr);
		StringBuffer tagSetSB = new StringBuffer();

    	measure = String.format("%s_%s", objectName.getDomain(), name);
		for (String k: m.keySet()){
				tagSetSB.append("," + k + "=" + m.get(k));
		}
		String timeStampS = "";
	
		if (withTimestamp != null){
			if (withTimestamp.equals("")){
				timeStampS = Long.toString(timeStamp * 1000 * 1000);
			} else {
				timeStampS = withTimestamp;
			}
			
		}
		
		String dateStamp = "";
		if (withDatestamp != null){
			dateStamp = (new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss] ")).format(new Date(timeStamp));
		}
		return String.format("%s%s%s %s=%s %s\n", dateStamp, measure, tagSetSB, attributeName, attributeValue, timeStampS);	
	}

}
