package brx.tools.jmxcli;

import java.util.logging.Logger;

import javax.el.ELException;

public class Expression {

    private String operator = null;
    private float value = -1;
    private static Logger LOGGER = Logger.getLogger(Expression.class.getName());

    public Expression(String operator, String value) throws ELException {
        if (!((operator.equals("gt") || operator.equals("lt") || operator.equals("eq") || operator.equals("true")))) {
            String msg = "Expression must be one of 'gt', 'eq', 'lt' or 'true'";
            ELException e = new ELException(msg);
            throw e;
        } else {
            this.operator = operator;
            this.value = Float.parseFloat(value);
        }
    }

    public boolean eval(String evalValue) {
        if (evalValue == null) {
            return false;
        }
        return this.eval(Float.parseFloat(evalValue));
    }
    
    public boolean eval(Object evalValue) {
        if (evalValue == null) {
            return false;
        }
        return this.eval(evalValue.toString());
    }

    public boolean eval(float evalValue) {
        switch (operator) {
        case "true":
            return true;
        case "gt":
            return evalValue > this.value;
        case "lt":
            return evalValue < this.value;
        case "eq":
            return evalValue == this.value;
        default:
            break;
        }
        return false;
    }
}
