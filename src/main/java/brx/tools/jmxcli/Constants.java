package brx.tools.jmxcli;

public interface Constants {
	static String MBEANSERVER_RUNTIME = "weblogic.management.mbeanservers.runtime";
	static String MBEANSERVER_DOMAINRUNTIME = "weblogic.management.mbeanservers.domainruntime";
	static String MBEANSERVER_EDIT = "weblogic.management.mbeanservers.edit";
}
