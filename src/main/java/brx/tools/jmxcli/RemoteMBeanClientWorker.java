package brx.tools.jmxcli;

import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import javax.management.AttributeList;
import javax.management.ObjectName;
import javax.naming.NamingException;

public class RemoteMBeanClientWorker implements Runnable {
	String operation =  "getAttributes";
	String[] attributes = null;
	String mbeanName = null;
	RemoteMBeanClient rmbc;
	private static Logger LOGGER = Logger.getLogger(Main.class.getName());
	Set<ObjectName> mbeans;

	
	public RemoteMBeanClientWorker(String serviceURLString, String principal, String credentials)
	{
		
		LOGGER.fine(Thread.currentThread().getName() + " - Service URL: " + serviceURLString);
		rmbc = new RemoteMBeanClient(serviceURLString, principal, credentials);
		LOGGER.info(Thread.currentThread().getName() + " - Connecting to: " + rmbc.getServiceUrl());
	}
	
	public void setQueryMBean(String mbeanName) throws NamingException{
		this.mbeanName = mbeanName;
		mbeans = rmbc.queryNames(mbeanName);
		if (mbeans == null) {
			throw new NamingException("MBean for '" + mbeanName + "' not found.");
		}
	}
	
	public void setQueryAttributes(String[] attributes){
		this.attributes = attributes;
	}
	
	
	public StringBuffer getAttributes(String[] attributes) {
		StringBuffer out = new StringBuffer();
		LOGGER.fine("ENTER - getAttributes(args)");
		//AttributeList attrList = (AttributeList) rmbc.getAttributes(mbeanName, attributes);
		return out;
	}
	
	public void run(){
		String name = Thread.currentThread().getName();
		if ((mbeanName == null) || (attributes == null)){
			LOGGER.severe(name + " - 'mbean' and 'attributes' must be set!");
			return;
		}
	}
}
