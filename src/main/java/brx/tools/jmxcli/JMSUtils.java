package brx.tools.jmxcli;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.management.ObjectName;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import brx.tools.jmxcli.Constants;
import brx.tools.jmxcli.multilineformatter.BasicMultilineFormatter;
import brx.tools.jmxcli.multilineformatter.YAMLCustomRepresenter;
import brx.tools.jmxcli.multilineformatter.YAMLMultilineFormatter;

public class JMSUtils {
	final static String QUERY_JMSSESSIONRUNTIME_BYLOCATION = "com.bea:Type=JMSSessionRuntime,Location=%s,*";
	final static String QUERY_JMSSESSIONRUNTIME_BYLOCATION_PARTITIONED = "com.bea:Type=JMSSessionRuntime,Location=%s,Name=session%d*,*";
	final static String QUERY_JMSCONNECTIONRUNTIME_BYLOCATIONANDNAME = "com.bea:Type=JMSConnectionRuntime,Location=%s,Name=%s,*";
	final static String QUERY_JMSDESTINATIONRUNTIME_BYNAME = "com.bea:Type=JMSDestinationRuntime,Name=%s,*";
	final static String QUERY_JMSCONSUMERRUNTIME_BYLOCATION = "com.bea:Type=JMSConsumerRuntime,Location=%s,*";
	final static String QUERY_JMSPRODUCERRUNTIME_BYLOCATION = "com.bea:Type=JMSProducerRuntime,Location=%s,*";
	final static String QUERY_JMSCONNECTIONRUNTIME_BYNAME = "com.bea:Type=JMSConnectionRuntime,Name=%s,*";
	final static String QUERY_JMSSESSIONRUNTIME_BYNAME = "com.bea:Type=JMSSessionRuntime,Name=%s,*";
	final static int PARTITIONS = 10;

	private static Yaml yaml;

	static RemoteMBeanClient remoteMBS;
	private static Logger LOGGER = Logger.getLogger(JMSUtils.class.getName());
	static String USAGE = "Usage: brx.tools.jmxcli.JMSUtils METHOD.";

	public static void listConsumersWithPendingBytesByHost(String mBeanFilter, long threshold) {
		final String BYTESPENDINGCOUNTSUM_KEY = "BytesPendingCount{SUM}";
		final String CONNECTIONCOUNT_KEY = "ConnectionsByHost";
		final String SESSIONSCOUNT_KEY = "SessionsByHost";
		final String CONNECTIONLIST_KEY = "ConnectionsList";
		LOGGER.fine("ENTER - listConsumersWithPendingBytesByHost(args)");
		HashSet<TreeMap<String, Object>> results = new HashSet<TreeMap<String, Object>>();
		results = listConsumersWithPendingBytes(mBeanFilter, threshold);
		TreeMap<String, TreeMap<String, Object>> out = new TreeMap<String, TreeMap<String, Object>>();

		for (TreeMap<String, Object> o : results) {
			TreeMap<String, Object> attrs = new TreeMap<String, Object>();
			attrs.put(BYTESPENDINGCOUNTSUM_KEY, 0L);
			attrs.put(SESSIONSCOUNT_KEY, 0L);
			String hostName = (String) o.get("HostName");
			String connection = (String) o.get("Connection");
			String serverRuntime = (String) o.get("ServerRuntime");
			Long sessions = (Long) o.get("SessionsCurrentCount");
			Long bpc = (Long) o.get("BytesPendingCount");
			LOGGER.finer("## hostName=" + hostName + " connection=" + connection + " sessions=" + sessions);
			TreeSet<String> connectionlist = new TreeSet<String>();
			if (out.containsKey(hostName)) {
				connectionlist = (TreeSet<String>) out.get(hostName).get(CONNECTIONLIST_KEY);
				attrs.put(BYTESPENDINGCOUNTSUM_KEY, (Long) out.get(hostName).get(BYTESPENDINGCOUNTSUM_KEY) + bpc);
				if (!connectionlist.contains(serverRuntime + ":" + connection)) {
					attrs.put(SESSIONSCOUNT_KEY, (Long) out.get(hostName).get(SESSIONSCOUNT_KEY) + sessions);
				}
			} else {
				attrs.put(BYTESPENDINGCOUNTSUM_KEY, bpc);
				attrs.put(SESSIONSCOUNT_KEY, sessions);
			}
			connectionlist.add(serverRuntime + ":" + connection);
			attrs.put(CONNECTIONLIST_KEY, connectionlist);
			out.put(hostName, attrs);
		}
		System.out.println("\n** Summary of BytesPendingCount by Consumer Host for " + mBeanFilter + " **");
		for (String hostName : out.keySet()) {
			System.out.println(String.format("%s %s %d %s %d %s %d", hostName, CONNECTIONCOUNT_KEY, ((TreeSet<String>) out.get(hostName).get(CONNECTIONLIST_KEY)).size(), BYTESPENDINGCOUNTSUM_KEY, out.get(hostName).get(BYTESPENDINGCOUNTSUM_KEY), SESSIONSCOUNT_KEY, out.get(hostName).get(SESSIONSCOUNT_KEY)));
		}
	}

	public static HashSet<TreeMap<String, Object>> listConsumersWithPendingBytes(String mBeanFilter, long threshold) {
		LOGGER.fine("ENTER - listConsumersWithPendingBytes(args)");
		return listEndpointWithPendingBytes("JMSConsumerRuntime", mBeanFilter, threshold);
	}

	public static HashSet<TreeMap<String, Object>> listProducersWithPendingBytes(String mBeanFilter, long threshold) {
		LOGGER.fine("ENTER - listConsumersWithPendingBytes(args)");
		return listEndpointWithPendingBytes("JMSProducerRuntime", mBeanFilter, threshold);
	}

	public static HashSet<TreeMap<String, Object>> listEndpointWithPendingBytes(String jmsEndpointType,
			String mBeanFilter, long threshold) {
		LOGGER.fine("ENTER - listEndpointWithPendingBytes(args)");
		HashSet<TreeMap<String, Object>> results = new HashSet<TreeMap<String, Object>>();
		Set<ObjectName> jmsEndpoint = new TreeSet<ObjectName>();
		if ((mBeanFilter == null) || (mBeanFilter.equals(""))) {
			mBeanFilter = "Name=*";
		}
		String query = String.format("com.bea:Type=%s,%s,*", jmsEndpointType, mBeanFilter);
		LOGGER.finer("query='" + query + "'");
		jmsEndpoint = remoteMBS.queryNames(query);
		LOGGER.finer(" result: " + jmsEndpoint.size());
		for (ObjectName jmsEndpointObjectName : jmsEndpoint) {
			Long bytesPendingCount = (Long) remoteMBS.getAttribute(jmsEndpointObjectName, "BytesPendingCount");
			LOGGER.finer(String.format("## %s=%s BytesPendingCount=%d", jmsEndpointType, jmsEndpointObjectName, bytesPendingCount));
			if ((bytesPendingCount != null) && (bytesPendingCount > threshold)) {
				String connection = jmsEndpointObjectName.getKeyProperty("JMSConnectionRuntime");
				String serverName = jmsEndpointObjectName.getKeyProperty("ServerRuntime");
				LOGGER.finer(String.format("## connection=%s BytesPendingCount=%d", connection, bytesPendingCount));
				Set<ObjectName> jmsConnections = new TreeSet<ObjectName>();
				String queryConnections = String.format("com.bea:Type=JMSConnectionRuntime,Name=%s,%s,*", connection, mBeanFilter);
		        LOGGER.finer("query='" + queryConnections + "'");
				jmsConnections = remoteMBS.queryNames(queryConnections);
				for (ObjectName connectionObjectName : jmsConnections) {
					TreeMap<String, Object> attrs = new TreeMap<String, Object>();
					String hostAddress = (String) remoteMBS.getAttribute(connectionObjectName, "HostAddress");
					String hostName;
					try {
						hostName = InetAddress.getByName(hostAddress).getHostName();
					} catch (UnknownHostException e) {
						LOGGER.warning(e.toString());
						hostName = hostAddress;
					}
					attrs.put("HostName", hostName);
					Long sessionsCurrentCount = (Long) remoteMBS.getAttribute(connectionObjectName, "SessionsCurrentCount");
					attrs.put("SessionsCurrentCount", sessionsCurrentCount);
					attrs.put("Connection", connection);
					attrs.put("BytesPendingCount", bytesPendingCount);
					attrs.put("ServerRuntime", serverName);
					results.add(attrs);
					System.out.println(String.format("serverruntime %s connection %s BytesPendingCount %d hostname %s sessionsCurrentCount %d", serverName, connection, bytesPendingCount, hostName, sessionsCurrentCount));
				}

			}
		}
		return results;

	}

	public static void getEndpointInfo(String type, String name, String filter) {
		LOGGER.fine("ENTER - getEndpointInfo(args)");
		String query = String.format("com.bea:Type=%s,Name=%s,%s,*", type, name, filter);
		HashSet<ObjectName> jmsEndpoint = (HashSet<ObjectName>) remoteMBS.queryNames(query);
		BasicMultilineFormatter multilineFormatter = new YAMLMultilineFormatter();

		for (ObjectName jmsEndpointObjectName : jmsEndpoint) {
			long timeStamp = System.nanoTime();
			// com.bea:Type=JMSProducerRuntime,Location=momw0Server_v8225,Name=producer385108,JMSSessionRuntime=session385107,JMSRuntime=momw0Server_v8225.jms,JMSConnectionRuntime=connection385105,ServerRuntime=momw0Server_v8225
			Hashtable<String, String> keysPropList = jmsEndpointObjectName.getKeyPropertyList();
			TreeMap<String, Object> attrMap = remoteMBS.getAllAttributesValues(jmsEndpointObjectName);
			System.out.println(multilineFormatter.format(jmsEndpointObjectName, attrMap, timeStamp));
			String[] keyComponents = { "JMSSessionRuntime", "JMSConnectionRuntime" };
			for (String key : keyComponents) {
				HashSet<ObjectName> jmsObjs = (HashSet<ObjectName>) remoteMBS.queryNames(
						String.format("com.bea:Type=%s,Name=%s,%s,*",key, keysPropList.get(key), filter));
				for (ObjectName mbeanName: jmsObjs){
					TreeMap<String, Object> map = remoteMBS.getAllAttributesValues(mbeanName);
					System.out.println(multilineFormatter.format(mbeanName, map, timeStamp));
				}
			}
		}

		LOGGER.fine("EXIT - getEndpointInfo(args)");
	}

	public static void listConsumersBySessionBytesPendingForServer(String serverName) {
		Set<ObjectName> jmsSessionRuntimes = new TreeSet<ObjectName>();
		for (int i = 0; i < 9; i++) {
			Set<ObjectName> tempSet = remoteMBS.queryNames(String.format(QUERY_JMSSESSIONRUNTIME_BYLOCATION_PARTITIONED, serverName, i));
			jmsSessionRuntimes.addAll(tempSet);
		}

		TreeMap<String, Long> bytesForConnectionMap = new TreeMap<String, Long>();
		for (ObjectName jmsSessionRuntime : jmsSessionRuntimes) {
			Long bytesPendingCount = (Long) remoteMBS.getAttribute(jmsSessionRuntime, "BytesPendingCount");
			LOGGER.finer("jmsSessionRuntime: " + jmsSessionRuntime.getCanonicalKeyPropertyListString() + " BytesPendingCount=" + bytesPendingCount);
			if ((bytesPendingCount != null) && (bytesPendingCount > 0L)) {
				LOGGER.finer("jmsSessionRuntime: " + jmsSessionRuntime.getCanonicalKeyPropertyListString());
				String connection = jmsSessionRuntime.getKeyProperty("JMSConnectionRuntime");
				Long value = bytesForConnectionMap.containsKey(connection) ? bytesForConnectionMap.get(connection) : 0;
				bytesForConnectionMap.put(connection, bytesPendingCount + value);
			}
		}
		jmsSessionRuntimes = null;
		TreeMap<String, Long> bytesForClientMap = new TreeMap<String, Long>();
		System.out.println("Bytes Pending by connection.");
		for (String connection : bytesForConnectionMap.keySet()) {
			Set<ObjectName> jmsConnectionRuntimes = remoteMBS.queryNames(String.format(QUERY_JMSCONNECTIONRUNTIME_BYLOCATIONANDNAME, serverName, connection));
			for (ObjectName jmsConnectionRuntime : jmsConnectionRuntimes) {
				String hostAddress = (String) remoteMBS.getAttribute(jmsConnectionRuntime, "HostAddress");
				String hostName;
				try {
					hostName = InetAddress.getByName(hostAddress).getHostName();
				} catch (UnknownHostException e) {
					LOGGER.warning(e.toString());
					hostName = hostAddress;
				}
				Long value = bytesForClientMap.containsKey(hostName) ? bytesForClientMap.get(hostName) : 0;
				bytesForClientMap.put(hostName, bytesForConnectionMap.get(connection) + value);
				LOGGER.fine(yaml.dump(remoteMBS.getAllAttributesValues(jmsConnectionRuntime)));
				System.out.println(connection + " " + bytesForConnectionMap.get(connection) + " " + hostName);
			}
		}
		System.out.println("Bytes Pending by consumer.");
		for (String hostName : bytesForClientMap.keySet()) {
			System.out.println(hostName + " " + bytesForClientMap.get(hostName) + " Bytes");
		}
	}

	public static void getConsumerIPforDestination(String destinationName) {
		TreeMap<String, String> destinationsMap = new TreeMap<String, String>();
		List<Map<String, Object>> destinationsInfo = new ArrayList<Map<String, Object>>();

		// get JMSDestinationRuntime: obtains Location and Name
		String query = String.format(QUERY_JMSDESTINATIONRUNTIME_BYNAME, destinationName);
		LOGGER.finer("query='" + query + "'");
		Set<ObjectName> jmsDestinationRuntimes = remoteMBS.queryNames(query);
		for (ObjectName jmsDestinationRuntime : jmsDestinationRuntimes) {
			LOGGER.finer("jmsDestinationRuntime: " + jmsDestinationRuntime.getCanonicalKeyPropertyListString());
			destinationsMap.put(jmsDestinationRuntime.getKeyProperty("Name"), jmsDestinationRuntime.getKeyProperty("Location"));
		}
		jmsDestinationRuntimes = null;

		// get jmsConsumerRuntime given the location and if DestinationName
		// matches obtains the Connection
		for (String destinationNameCurrent : destinationsMap.keySet()) {
			String location = destinationsMap.get(destinationNameCurrent);
			LOGGER.fine("Looking for DestinationName: " + destinationNameCurrent + " at " + location);
			Set<ObjectName> jmsConsumerRuntimes = remoteMBS.queryNames(String.format(QUERY_JMSCONSUMERRUNTIME_BYLOCATION, location));
			for (ObjectName jmsConsumerRuntime : jmsConsumerRuntimes) {
				String jmsConsumerDestinationCurrent = (String) remoteMBS.getAttribute(jmsConsumerRuntime, "DestinationName");
				LOGGER.finer(jmsConsumerRuntime.getCanonicalName() + " " + jmsConsumerDestinationCurrent + " ? destinationNameCurrent");
				if (jmsConsumerDestinationCurrent.equals(destinationNameCurrent)) {
					LOGGER.fine("Found: " + jmsConsumerRuntime.getCanonicalKeyPropertyListString());
					TreeMap<String, Object> map = new TreeMap<String, Object>();
					map.put("DestinationName", (String) destinationNameCurrent);
					map.put("Location", (String) location);
					map.put("Consumer", (String) jmsConsumerRuntime.getKeyProperty("Name"));
					map.put("Connection", jmsConsumerRuntime.getKeyProperty("JMSConnectionRuntime"));
					LOGGER.finer("{\n\tDestinationName: " + destinationNameCurrent + "\n\tLocation: " + location + "\n\tConsumer: " + jmsConsumerRuntime.getKeyProperty("Name") + "\n\tSession: " + jmsConsumerRuntime.getKeyProperty("JMSSessionRuntime") + "\n\tConnection: " + jmsConsumerRuntime.getKeyProperty("JMSConnectionRuntime") + "\n}");
					Set<String> IPs = new HashSet<String>();
					// given the Connection, obtain the IP
					LOGGER.fine("  Looking into JMSConnectionRuntime: " + jmsConsumerRuntime.getKeyProperty("JMSConnectionRuntime"));
					for (ObjectName o : remoteMBS.queryNames(String.format(QUERY_JMSCONNECTIONRUNTIME_BYNAME, jmsConsumerRuntime.getKeyProperty("JMSConnectionRuntime")))) {
						LOGGER.fine(o.getCanonicalKeyPropertyListString());
						String ip = (String) remoteMBS.getAttribute(o, "HostAddress");
						LOGGER.finer("HostAddress: " + ip);
						IPs.add(ip);
					}
					LOGGER.fine("  Bytes pending for session " + jmsConsumerRuntime.getKeyProperty("JMSSessionRuntime"));
					for (ObjectName o : remoteMBS.queryNames(String.format(QUERY_JMSSESSIONRUNTIME_BYNAME, jmsConsumerRuntime.getKeyProperty("JMSSessionRuntime")))) {
						LOGGER.fine(o.getCanonicalKeyPropertyListString());
						map.put("Session", o.getCanonicalKeyPropertyListString());
						map.put("BytesPendingCount", (Long) remoteMBS.getAttribute(o, "BytesPendingCount"));
					}
					String HostAddress = "";
					for (String s : IPs) {
						HostAddress = s + " " + HostAddress;
					}
					map.put("HostAddresses", HostAddress);

					destinationsInfo.add(map);
					map = null;
				}
			}
		}

		System.out.println("DestinationName Location HostAddresses BytesPendingCount Session");
		for (Map<String, Object> map : destinationsInfo) {
			System.out.println(map.get("DestinationName") + " " + map.get("Location") + " " + map.get("HostAddresses") + " " + map.get("BytesPendingCount") + " " + map.get("Session"));
		}
	}

	public static void listConsumersAll() {
		final String JMSSERVERS_QUERY = "com.bea:Type=JMSRuntime,*";
		Set<ObjectName> jmsServers = remoteMBS.queryNames(JMSSERVERS_QUERY);
		TreeMap<String, TreeMap> results = new TreeMap<String, TreeMap>();

		System.err.println("WARNING: THIS METHOD COULD SLOW DOWN AND HARM YOUR TARGET SERVICE. Press a key to continue.");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (ObjectName jmsServer : jmsServers) {
			System.err.print("J");
			String name = (String) remoteMBS.getAttribute(jmsServer, "Name");
			LOGGER.fine(" - jmsServer: '" + name + "'");
			TreeMap<String, Object> data = new TreeMap<String, Object>();
			results.put(name, data);
			Set<ObjectName> connections = remoteMBS.queryNames("com.bea:Type=JMSConnectionRuntime,JMSRuntime=" + name + ",*");
			for (ObjectName connection : connections) {
				System.err.print("c");
				ObjectName[] sessions = (ObjectName[]) remoteMBS.getAttribute(connection, "Sessions");
				if (sessions == null) {
					continue;
				}
				for (ObjectName session : sessions) {
					System.err.print("s");
					TreeMap<String, Object> sessionAttr = (TreeMap<String, Object>) remoteMBS.getAllAttributesValues(session);
					Long bytesPendingCount = (Long) sessionAttr.get("BytesPendingCount");
					if (bytesPendingCount == 0) {
						continue;
					}
					String connName = (String) remoteMBS.getAttribute(connection, "Name");
					String hostAddress = (String) remoteMBS.getAttribute(connection, "HostAddress");
					LOGGER.fine("sessionAttr: " + sessionAttr);
					String sessName = (String) sessionAttr.get("Name");
					LOGGER.fine(" -   connection: '" + connName + "' hostAddress: '" + hostAddress + "'");
					Long ConsumersCurrentCount = (Long) sessionAttr.get("ConsumersCurrentCount");
					Long ProducersCurrentCount = (Long) sessionAttr.get("ProducersCurrentCount");
					LOGGER.fine(" -     session: '" + sessName + "' BytesPendingCount: '" + bytesPendingCount + "'");
					LOGGER.fine("                'Consumers: '" + ConsumersCurrentCount + "'");
					StringBuffer mDests = new StringBuffer();
					for (ObjectName consumer : (ObjectName[]) sessionAttr.get("Consumers")) {
						String mDest = (String) remoteMBS.getAttribute(consumer, "MemberDestinationName");
						LOGGER.fine("                  'MemberDestinationName: '" + mDest + "'");
						mDests.append("#" + mDest);
					}
					LOGGER.fine("                'Producers: '" + ProducersCurrentCount + "'");
					Object[] args = new Object[] { hostAddress, connName, sessName, ProducersCurrentCount, ConsumersCurrentCount, mDests, bytesPendingCount };
					System.out.printf("\nip=%s n=%s s=%s p=%d c=%d d=%s value=%d\n", args);
				}
			}
		}

	}

	public static void main(String[] args) throws Exception {
		String SECURITY_PRINCIPAL = System.getProperty("SECURITY_PRINCIPAL");
		String SECURITY_CREDENTIALS = System.getProperty("SECURITY_CREDENTIALS");
		String HOST = System.getProperty("HOST");
		String PORT = System.getProperty("PORT");
		String mbeanServer = System.getProperty("MBEANSERVER");

		if (mbeanServer == null) {
			mbeanServer = Constants.MBEANSERVER_DOMAINRUNTIME;
		}
		String serviceURLString = "service:jmx:t3://" + HOST + ":" + PORT + "/jndi/" + mbeanServer;
		String method = "";
		if (args.length > 0) {
			method = args[0];
		}
		try {
			LOGGER.fine("Service URL: " + serviceURLString);
			remoteMBS = new RemoteMBeanClient(serviceURLString, SECURITY_PRINCIPAL, SECURITY_CREDENTIALS);
			LOGGER.info("Connecting to: " + remoteMBS.getServiceUrl());

			DumperOptions yamlOpt = new DumperOptions();
			yamlOpt.setDefaultFlowStyle(DumperOptions.FlowStyle.AUTO);
			yamlOpt.setPrettyFlow(true);
			yaml = new Yaml(new YAMLCustomRepresenter(), yamlOpt);

			switch (method) {
			case "listConsumersAll":
				listConsumersAll();
				break;
			case "getConsumerIPforDestination":
				getConsumerIPforDestination(args[1]);
				break;
			case "listConsumersBySessionBytesPendingForServer":
				listConsumersBySessionBytesPendingForServer(args[1]);
				break;
			case "listConsumersWithPendingBytes":
				listConsumersWithPendingBytes(args[1], Long.parseLong(args[2]));
				break;
			case "listConsumersWithPendingBytesByHost":
				listConsumersWithPendingBytesByHost(args[1], Long.parseLong(args[2]));
				break;
			case "getEndpointInfo":
				getEndpointInfo(args[1], args[2], args[3]);
				break;
			default:
				System.err.println("ERROR: method '" + method + "' not implemented yet.");
				System.err.println(USAGE);
				System.err.println("Implemented methods are:");
				for (java.lang.reflect.Method m : JMSUtils.class.getDeclaredMethods()) {
					if (m.getName().startsWith("main") || !(java.lang.reflect.Modifier.isPublic(m.getModifiers()))) {
						continue;
					}
					System.err.print("\t" + m.getName().substring(m.getName().lastIndexOf('.') + 1));
					for (Class p : m.getParameterTypes()) {
						String name = p.getSimpleName();
						// if(p.isNamePresent()){
						// name = p.getName();
						// }
						System.err.print(" " + name);
					}
					System.err.println();
				}
			}
		} catch (Exception e) {
			remoteMBS.close();
			e.printStackTrace();
		}
		remoteMBS.close();
	}

}
