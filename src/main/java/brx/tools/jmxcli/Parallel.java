package brx.tools.jmxcli;

import java.util.Arrays;
import java.util.logging.Logger;

import javax.naming.NamingException;

public class Parallel {

	static String USAGE = "Usage: brx.tools.jmxcli.Parallel MBEANNAME ATTRIBUTES SERVER:PORT [SERVER:PORT ...]";

	static String MBEANSERVER_RUNTIME = "weblogic.management.mbeanservers.runtime";
	static String MBEANSERVER_DOMAINRUNTIME = "weblogic.management.mbeanservers.domainruntime";
	static String MBEANSERVER_EDIT = "weblogic.management.mbeanservers.edit";
	private static Logger LOGGER = Logger.getLogger(Parallel.class.getName());
	static private int EXIT_NOT_FOUND = 2;

	public static void main(String[] args) {
		
		if (args.length < 3){
			System.out.println(USAGE);
			System.exit(1);
		}
		Thread[] threadPool = new Thread[args.length-2];
		
		String SECURITY_PRINCIPAL = System.getProperty("SECURITY_PRINCIPAL");
		String SECURITY_CREDENTIALS = System.getProperty("SECURITY_CREDENTIALS");
		String mbeanServer = System.getProperty("MBEANSERVER");
		
		
		if (mbeanServer == null) {
			mbeanServer = MBEANSERVER_RUNTIME;
		}
		String mbeanName = args[0];
		String[] attributes = args[1].split(",");
		String[] serverPorts =  Arrays.copyOfRange(args, 2, args.length);
		int t = 0;
		for ( String serverPort: serverPorts){
				String serviceURLString = "service:jmx:t3://" + serverPort + "/jndi/" + mbeanServer;
				LOGGER.fine("Creating Worker for Service URL: " + serviceURLString);
				RemoteMBeanClientWorker r = new RemoteMBeanClientWorker(serviceURLString, SECURITY_PRINCIPAL, SECURITY_CREDENTIALS);
				try {
					r.setQueryMBean(mbeanName);
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				r.setQueryAttributes(attributes);
				String threadName = "MBeanClientWorker-" + serverPort;
				LOGGER.fine("Creating Thread [" + t + "]: " + threadName);
				threadPool[t] = new Thread(r);
				threadPool[t].setName(threadName);
		}
		
		for (Thread thread: threadPool){
			thread.start();
		}
		LOGGER.info("All thread started...");
		for (Thread thread: threadPool){
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LOGGER.info("All thread finished...");
		
	}

}
