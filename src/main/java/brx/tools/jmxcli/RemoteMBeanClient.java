package brx.tools.jmxcli;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.TreeMap;
import java.util.Hashtable;
import java.util.Set;
import java.util.TreeSet;

import javax.management.*;
import javax.management.remote.*;

import java.util.logging.*;

public class RemoteMBeanClient {

	private JMXConnector jmxCon = null;
	private JMXServiceURL serviceUrl = null;
	private MBeanServerConnection mbsconn = null;
	private static Logger LOGGER = Logger.getLogger(RemoteMBeanClient.class.getName());

	private String lastMessage = null;
	public long timerRemoteMBeanConnect = 0l;
	public long timerQueryNames = 0l;
	public long timerGetAttribute = 0l;
	private long timer = 0l;
	
	public JMXServiceURL getServiceUrl() {
		return serviceUrl;
	}

	public RemoteMBeanClient(String serviceURLString, String principal, String credentials) {
		LOGGER.finer("ENTER - CONSTRUCTOR");
		long timer = System.nanoTime();
		try {
			serviceUrl = new JMXServiceURL(serviceURLString);
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
			env.put(javax.naming.Context.SECURITY_PRINCIPAL, principal);
			env.put(javax.naming.Context.SECURITY_CREDENTIALS, credentials);
			jmxCon = JMXConnectorFactory.newJMXConnector(serviceUrl, env);
			jmxCon.connect();
			mbsconn = jmxCon.getMBeanServerConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.timerRemoteMBeanConnect = (System.nanoTime() - timer);
		LOGGER.finer("EXIT - CONSTRUCTOR");
	}
	
	public void close(){
            mbsconn = null;
            try {
            	if (jmxCon != null){
                	jmxCon.close();
                	jmxCon = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
	}

	public Set<ObjectName> queryNames() {
		long timer = System.nanoTime();
		try {
			return mbsconn.queryNames(null, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.timerQueryNames = (System.nanoTime() - timer);
		return null;
	}

	public Set<ObjectName> queryNames(String name) {
		LOGGER.finer("ENTER - queryNames(name)");
		long timer = System.nanoTime();
		ObjectName oname = null;
		try {
			if (name != null) {
				oname = new ObjectName(name);
				Set<ObjectName> result = mbsconn.queryNames(oname, null);
				this.timerQueryNames = (System.nanoTime() - timer);
				this.timer = this.timerQueryNames;
				LOGGER.finer("EXIT - queryNames (" + result.size() + ").");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.timerQueryNames = (System.nanoTime() - timer);
		LOGGER.finer("EXIT - queryNames");
		return null;
	}
	
	public Set<ObjectName> queryNamesWithPartitions(String queryTemplate, int partnum) {
		LOGGER.finer("ENTER - queryNamesWithPartitions((name)");
        Set<ObjectName> partitions = new TreeSet<ObjectName>();
        for (int i = 0; i < partnum; i++){
            Set<ObjectName> tempSet = this.queryNames(String.format(queryTemplate, i));
            partitions.addAll(tempSet);
        }
		LOGGER.finer("EXIT - queryNamesWithPartitions(");
		return partitions;
	}

	private StringBuffer formatMBeanFeatures(MBeanFeatureInfo[] mbfiList) {
		StringBuffer info = new StringBuffer();
		for (MBeanFeatureInfo mbfi : mbfiList) {
			if (mbfi instanceof MBeanAttributeInfo) {
				info.append("\t- " + mbfi.getName() + "[" + ((MBeanAttributeInfo) mbfi).getType() + "]: '"
						+ mbfi.getDescription() + "'\n");
			}
			if (mbfi instanceof MBeanOperationInfo) {
				info.append("\t- " + mbfi.getName() + "(): '" + mbfi.getDescription() + "'\n");
				info.append("\t\t- signature: \n");
				for (MBeanParameterInfo mbpi : ((MBeanOperationInfo) mbfi).getSignature()) {
					info.append(
							"\t\t\t- " + mbpi.getName() + "[" + mbpi.getType() + "]: " + mbpi.getDescription() + "\n");
				}
				info.append("\t\t- returns: [" + ((MBeanOperationInfo) mbfi).getReturnType() + "]\n");
			}
		}
		return info;
	}

	public ObjectInstance getObjectInstance(ObjectName oname) {
		ObjectInstance o;
		try {
			o = mbsconn.getObjectInstance(oname);
		} catch ( InstanceNotFoundException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return o;
	}

	public String getMBeanInfo(ObjectName oname) {
		StringBuffer info = new StringBuffer();
		info.append("name: '" + oname.getCanonicalName() + "'\n");

		MBeanInfo mbi;
		try {
			mbi = mbsconn.getMBeanInfo(oname);
		} catch (InstanceNotFoundException | IntrospectionException | ReflectionException | IOException e) {
			e.printStackTrace();
			return null;
		}
		info.append("  - description: '" + mbi.getDescription() + "'\n");
		info.append("  - attributes:\n");
		info.append(formatMBeanFeatures(mbi.getAttributes()));
		info.append("  - operations:\n");
		info.append(formatMBeanFeatures(mbi.getOperations()));
		return info.toString();
	}

	public String getMBeanInfo(String name) {
		ObjectName oname;
		try {
			oname = new ObjectName(name);
			return getMBeanInfo(oname);
		} catch (MalformedObjectNameException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Object invoke(ObjectName oname, String operationName, Object[] params, String[] signature) {
		LOGGER.finer("ENTER - invoke(args)");
		Object value = null;
		try {
			value = mbsconn.invoke(oname, operationName, params, signature);
		} catch (javax.management.InstanceNotFoundException infe){
			lastMessage = "javax.management.InstanceNotFoundException.";
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.finer("EXIT - invoke(args)");
		return value;
	}

	public Object getAttribute(ObjectName oname, String attribute) {
		LOGGER.finer("ENTER - getAttribute(args)");
		Object value = null;
		try {
			long timer = System.nanoTime();
			value = mbsconn.getAttribute(oname, attribute);
			this.timerGetAttribute = (System.nanoTime() - timer);
		} catch (javax.management.InstanceNotFoundException infe){
			lastMessage = "javax.management.InstanceNotFoundException.";
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.finer("EXIT - getAttribute(args)");
		return value;
	}
	
	public Object getAttribute(String name, String attribute) {
		LOGGER.finer("ENTER - getAttribute(args)");
		Object value = null;
		try {
			ObjectName oname = new ObjectName(name);
			value = mbsconn.getAttribute(oname, attribute);
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.finer("EXIT - getAttribute(args)");
		return value;
	}

	public TreeMap<String, Object> getAttributesValues(ObjectName oname, String[] attributes) {
		LOGGER.finer("ENTER - getAttributes(args)");
		TreeMap<String, Object> attrMap = new TreeMap<String, Object>();
		AttributeList attrList = null;
		try {
			attrList = mbsconn.getAttributes(oname, attributes);
			for (Attribute attr: attrList.asList()){
				attrMap.put(attr.getName(), attr.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.finer("EXIT - getAttribute(args)");
		return attrMap;
	}
	
	public TreeMap<String, Object> getAttributesValues(String name, String[] attributes) {
		LOGGER.finer("ENTER - getAttributes(args)");
		TreeMap<String, Object> value = null;
		try {
			ObjectName oname = new ObjectName(name);
			return this.getAttributesValues(oname, attributes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public TreeMap<String, Object> getAllAttributesValues(ObjectName oname) {
		// TreeMap<String, TreeMap> objMap = new TreeMap<String, TreeMap>();
		TreeMap<String, Object> attrMap = new TreeMap<String, Object>();
		try {
			MBeanInfo mbi = mbsconn.getMBeanInfo(oname);
			MBeanAttributeInfo[] attrInfo = mbi.getAttributes();
			for (MBeanAttributeInfo attr : attrInfo) {
				LOGGER.finer("attribute: '" + attr.getName() + "'");
				LOGGER.finer("  type: '" + attr.getType() + "'");
				LOGGER.finer("  value: '" + mbsconn.getAttribute(oname, attr.getName()) + "'");
				attrMap.put(attr.getName(), mbsconn.getAttribute(oname, attr.getName()));
			}
			// objMap.put(oname.toString(), attrMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return attrMap;
	}

	public String getLastMessage() {
		return lastMessage;
	}

	public long getLastTimer(){
		return timer;
	}
	
}