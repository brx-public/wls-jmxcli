package brx.tools.jmxcli;

import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.management.ObjectName;

import org.apache.commons.lang.ArrayUtils;

import brx.tools.jmxcli.*;

import weblogic.management.runtime.JTATransaction;

public class JTA {
	static RemoteMBeanClient remoteMBS;

	static String USAGE = "Usage: brx.tools.jmxcli.Utils METHOD.\n  valid methods are: ";

	static String MBEAN_JTARUNTIME = "com.bea:*,Type=JTARuntime";

	static String[] TX_ATTRS = {"xid", "status", "coordinatorURL", "secondsActive", "serversStatus", "resources"};
	
	private static Logger LOGGER = Logger.getLogger(Utils.class.getName());
	
	
	private JTATransaction[] getAllTransactions(){
		LOGGER.fine("ENTER - getAllTransactions()");
		Set<ObjectName> mbeans = remoteMBS.queryNames(MBEAN_JTARUNTIME);
		JTATransaction[] allActiveTransactions = new JTATransaction[0];
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				allActiveTransactions = (JTATransaction[]) ArrayUtils.addAll(allActiveTransactions, (JTATransaction[]) remoteMBS.getAttribute(mbeanName, "JTATransactions"));
			}
		}
		return allActiveTransactions;

	}
	
	public static void printTransactions() {
		LOGGER.fine("ENTER - printTransactions()");
		Set<ObjectName> mbeans = remoteMBS.queryNames(MBEAN_JTARUNTIME);
		if (mbeans != null) {
			for (ObjectName mbeanName : mbeans) {
				JTATransaction[] allActiveTransactions = (JTATransaction[]) remoteMBS.getAttribute(mbeanName, "JTATransactions");
				LOGGER.fine("printTransactions() - '" + mbeanName + "'.'" + "JTATransactions" + "' = '" + allActiveTransactions.toString() + "'");
				if (allActiveTransactions != null) {
					for (int i = 0; i < allActiveTransactions.length; i++) {
						JTATransaction tx = allActiveTransactions[i];
						LOGGER.fine("printTransactions() - processing tx " + tx.toString());
						int secondsActive = tx.getSecondsActiveCurrentCount();
						int txSecondsActiveMin = 0;
						String txSecondsActiveMinS = System.getProperty("PRINT_TX_SECONDSACTIVE_MIN");
						if (txSecondsActiveMinS != null){
							txSecondsActiveMin = Integer.parseInt(txSecondsActiveMinS);
						}
						if ((txSecondsActiveMin > 0) && (secondsActive < txSecondsActiveMin)) {
							continue;
						}
						System.out.println("- " + mbeanName.toString());
						System.out.print("  xid: ");
						try {
							System.out.println(tx.getXid().toString());
						} catch (Exception ex) {
							System.out.println("XID wrong or missing.");
						}
						System.out.println("  status: " + tx.getStatus());
						System.out.println("  coordinatorURL: " + tx.getCoordinatorURL());
						System.out.println("  name: " + tx.getName());
						System.out.println("  secondsActive: " + secondsActive);
						String[] txServers = tx.getServers();
						if (txServers == null)
							txServers = new String[0];
						System.out.println("  servers: ");
						for (String txServer : txServers) {
							System.out.println("    - " + txServer);
						}
	
						if (System.getProperty("PRINT_TX_RESOURCES") != null) {

							Map<String, Object> mapResourceNamesAndStatus = tx.getResourceNamesAndStatus();
							System.out.print("  resources: ");
							if (mapResourceNamesAndStatus != null) {
								System.out.println();
								System.out.println(Utils.printMap(mapResourceNamesAndStatus, 4));
							} else
								System.out.println("null");
						}
	
						if (System.getProperty("PRINT_TX_SERVERSSTATUS") != null) {
							Map<String, Object> mapServersAndStatus = tx.getServersAndStatus();
							System.out.print("  serversStatus: ");
							if (mapServersAndStatus != null) {
								System.out.println();
								System.out.println(Utils.printMap(mapServersAndStatus, 4));
							} else
								System.out.println("null");
						}
						if (System.getProperty("PRINT_TX_USERPROPERTIES") != null) {
							Map<String, Object> mapUserProperties = tx.getUserProperties();
							System.out.print("  userProperties: ");
							if (mapUserProperties != null) {
								System.out.println();
								System.out.println(Utils.printMap(mapUserProperties, 4));
							} else
								System.out.println("null");
						}
					}
	
				}
			}
		}
		LOGGER.fine("EXIT - printTransactions()");
	}

	public static void main(String[] args) throws Exception {
		String SECURITY_PRINCIPAL = System.getProperty("SECURITY_PRINCIPAL");
		String SECURITY_CREDENTIALS = System.getProperty("SECURITY_CREDENTIALS");
		String HOST = System.getProperty("HOST");
		String PORT = System.getProperty("PORT");
		String mbeanServer = System.getProperty("MBEANSERVER");
		if (mbeanServer == null) {
			mbeanServer = Constants.MBEANSERVER_DOMAINRUNTIME;
		}
		String serviceURLString = "service:jmx:t3://" + HOST + ":" + PORT + "/jndi/" + mbeanServer;
		String method = "";
		if (args.length > 0) {
			method = args[0];
		}
		try {
			LOGGER.fine("Service URL: " + serviceURLString);
			remoteMBS = new RemoteMBeanClient(serviceURLString, SECURITY_PRINCIPAL, SECURITY_CREDENTIALS);
			LOGGER.info("Connecting to: " + remoteMBS.getServiceUrl());
			switch (method) {
			case "printTransactions":
				printTransactions();
				break;
			default:
				System.err.println("ERROR: method '" + method + "' not implemented yet.");
				System.err.println(USAGE);
				System.exit(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
