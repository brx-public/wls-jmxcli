package brx.tools.jmxcli;
import java.util.Map;
import java.util.logging.Logger;
import javax.management.ObjectName;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Formatter{

	protected String withTimestamp = System.getProperty("Formatter.TimeStamp");
	protected String withDatestamp = System.getProperty("Formatter.DateStamp");
	protected String namingAttribute = System.getProperty("Formatter.NamingAttribute");

	private String SEPARATOR = " ";

	public abstract String format(ObjectName objectName, String attributeName, String attributeValue, long timeStampMillis);	

	protected Logger LOGGER = Logger.getLogger(this.getClass().getName());

	protected String getSeparator(){
		return SEPARATOR;
	}

	protected String formatTimeStampNano(long timeStampMillis){
		LOGGER.finer("(long) ENTER");
		String timeStampS = "";
		if (withTimestamp != null){
			if (withTimestamp.equals("")){
				timeStampS = Long.toString(timeStampMillis * 1000 * 1000);
			} else {
				timeStampS = withTimestamp;
			}
		}
		LOGGER.finer("EXIT");
		return timeStampS;
	};

	protected String formatDateStamp(long timeStampMillis){
		LOGGER.finer("(long) ENTER");
		String dateStamp = "";
		LOGGER.finer("timeStampMillis: " + timeStampMillis);
		LOGGER.finer("withDatestamp: " + withDatestamp);
		if (withDatestamp != null){
			dateStamp = (new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]")).format(new Date(timeStampMillis))
				+ getSeparator();
		}		
		LOGGER.finer("EXIT");
		return dateStamp;
	}

};
