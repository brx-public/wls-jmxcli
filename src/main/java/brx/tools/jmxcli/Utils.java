package brx.tools.jmxcli;

import java.util.Map;
import java.util.logging.Logger;

import brx.tools.jmxcli.*;

public class Utils<RemoteMBean> {
	private static Logger LOGGER = Logger.getLogger(Utils.class.getName());

	public static String printMap(Map<String, Object> map) {
		return printMap(map, 0);
	}

	public static String printMap(Map<String, Object> map, int indent) {
		String IND = String.format("%1$" + indent + "s", " ");
		StringBuffer out = new StringBuffer();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			out.append(IND + entry.getKey() + ": " + entry.getValue().toString() + "\n");
		}
		return out.toString();
	}

	public static String composeMBeanQueryWithFilter(String mbean_query, String mBeanFilter) {
		String query = mbean_query;
		if ((mBeanFilter != null) && !(mBeanFilter.equals(""))) {
			query = query + "," + mBeanFilter;
		}
		query = query + ",*";
		LOGGER.finer("mbean_query='" + mbean_query + "'");
		return query;
	}
}
