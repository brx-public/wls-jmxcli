package brx.tools.jmxcli;

import static org.junit.Assert.*;

import javax.el.ELException;

import org.junit.Test;

public class ExpressionTest  {

	public ExpressionTest() throws ELException {
		// TODO Auto-generated constructor stub
	}

	@Test
	public void testExpressionIntGtInt() {
		Expression exp = new Expression("gt", "1");
		assertTrue(exp.eval("2"));
	}

	@Test
	public void testExpressionFloatGtInt() {
		Expression exp = new Expression("gt", "1");
		assertTrue(exp.eval("1.1"));
	}
	
	@Test
	public void testExpressionNotFloatGtInt() {
		Expression exp = new Expression("gt", "2");
		assertFalse(exp.eval("1.1"));
	}

}
